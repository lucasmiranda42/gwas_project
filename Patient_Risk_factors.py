import argparse, vcf
import numpy as np

parser = argparse.ArgumentParser(description="Given a patient's VCF and a list of\
 variants to take into account, outputs the resulting risk factor for each trait of interest")
parser.add_argument('--GWAScat_vars', '-g', type=str, help='Annotated VCF containing all variants of interest')
parser.add_argument('--patient', '-p', type=str, help='VCF file containing data from a patient')
parser.add_argument('--NGS', '-n', action='store_true', help='True if the VCF comes from a sequencing experiment. Takes coverage into account')
parser.add_argument('--LowConfidence', '-lc', action='store_true', help='True for leaving out variants with low confidente (i.e. DP<10). Depends on NGS argument')

args = parser.parse_args()
Patient_data = args.patient
GWAS_vars = args.GWAScat_vars
NGS = args.NGS
LC = args.LowConfidence

#Establishes argument dependency
if LC and not NGS:
    raise Exception('LowConfidence parameter depends on NGS. See help for details')

#Loads Annotated VCF in order to parse all variants of interest
GWAS_vars = vcf.Reader(open(GWAS_vars,'r'))
GWAS_vars = [record for record in GWAS_vars]

#Builds a dictionary containing pre-calculated odds ratios for every possible
#genotype of each variant of interest, and a dictionary containing all variants
#of interest per trait
OR_dict = {}
Trait_dict = {}

for record in GWAS_vars:

    #Updates OR_dict
    OR_dict[record.ID] = [record.INFO['GWAS_DD'][0],record.INFO['GWAS_DH'][0],\
    record.INFO['GWAS_HH'][0]]

    #Updates Trait_dict
    try:
        Trait_dict[record.INFO['GWAS_TRAIT'][0]].append(record.ID)
    except KeyError:
        Trait_dict[record.INFO['GWAS_TRAIT'][0]] = [record.ID]


#Loads patient's VCF and keeps track of the cigosity of each variant.
print('Parsing patient data...')

Patient_data = vcf.Reader(open(Patient_data,'r'))
Patient_dict = {}

for record in Patient_data:
    if record.ID in OR_dict.keys():
        Patient_dict[record.ID] = []
        try:
            Patient_dict[record.ID].append(record.INFO['AF'][0])
        except KeyError:
            Patient_dict[record.ID].append(0.0)
        try:
            Patient_dict[record.ID].append(record.FILTER[0])
        except TypeError:
            Patient_dict[record.ID].append('.')

print('Parsing complete!')

#Next, defines a dictionary with traits as keys and the corresponding OR, given
#the genotypes of each variant
OR_per_trait = {traits:[] for traits in Trait_dict.keys()}

for traits,variants in Trait_dict.items():
    for variant in variants:

        try:
            if Patient_dict[variant][0] == 1.0:
                if not NGS:
                    OR_per_trait[traits].append(OR_dict[variant][0])
                elif NGS and Patient_dict[variant][1] != 'MissingData':
                    if LC and Patient_dict[variant][1] != 'LowConfidence':
                        OR_per_trait[traits].append(OR_dict[variant][0])
                    elif LC:
                        OR_per_trait[traits].append(1.0)
                    else:
                        OR_per_trait[traits].append(OR_dict[variant][0])
                else:
                    OR_per_trait[traits].append(1.0)

            elif Patient_dict[variant][0] == 0.5:
                if not NGS:
                    OR_per_trait[traits].append(OR_dict[variant][1])
                elif NGS and Patient_dict[variant][1] != 'MissingData':
                    if LC and Patient_dict[variant][1] != 'LowConfidence':
                        OR_per_trait[traits].append(OR_dict[variant][1])
                    elif LC:
                        OR_per_trait[traits].append(1.0)
                    else:
                        OR_per_trait[traits].append(OR_dict[variant][1])
                else:
                    OR_per_trait[traits].append(1.0)

            elif Patient_dict[variant][0] == 0.0:
                if not NGS:
                    OR_per_trait[traits].append(OR_dict[variant][2])
                elif NGS and Patient_dict[variant][1] != 'MissingData':
                    if LC and Patient_dict[variant][1] != 'LowConfidence':
                        OR_per_trait[traits].append(OR_dict[variant][2])
                    elif LC:
                        OR_per_trait[traits].append(1.0)
                    else:
                        OR_per_trait[traits].append(OR_dict[variant][2])
                else:
                    OR_per_trait[traits].append(1.0)

        except KeyError:
            OR_per_trait[traits].append(1.0)

#Finally, builds a dictionary with the adjusted risk factor for every trait of interest
Risk_factor_per_trait = {key:np.prod(np.array(value)) for key,value in OR_per_trait.items()}

print(Risk_factor_per_trait)

#plots a histogram of the values
import matplotlib.pyplot as plt
plt.style.use('ggplot')
x = [i for i in Risk_factor_per_trait.values()]
plt.hist(x, bins=150)
plt.xlabel('OR*')
plt.ylabel('Counts')
plt.show()
