import argparse, re
import pandas as pd
import numpy as np
from tqdm import tqdm
from multiprocessing import cpu_count, Pool

def get_ref(alleles):
    """Given a set of alleles, returns ref"""
    try:
        return alleles[1]
    except TypeError:
        return np.nan

def get_alt(alleles):
    """Given a set of alleles, returns alt"""
    try:
        return alleles[3]
    except TypeError:
        return np.nan

def complement(genotype, reverse=False):
    """Returns the complement of the given genotype. Works both in raw and Alleles format
    (for example, GA will return CT and [G/A] will return [C/T])"""
    complement_dict = {'A':'T','T':'A','C':'G','G':'C','[':'[','/':'/',']':']','-':'-','I':'I','D':'D'}
    genotype = list(genotype)
    genotype = [complement_dict[nt] for nt in genotype]
    if reverse==True:
        return ''.join(genotype)[::-1]
    return ''.join(genotype)

def genotype(REF,ALT,GT,PosID):
    """Returns genotype information in a VCF compatible format"""

    #If position is not covered, return ./.
    if GT == '--':
        return './.'

    elif 'D' in GT or 'I' in 'GT':
        if len(REF) > len(ALT): #Deletion
            REF, ALT = 'I','D'
        elif len(REF) < len(ALT): #Insertion
            REF, ALT = 'D','I'

    alt_count = 0

    for i in GT:
        if i!=REF:
            alt_count += 1
    if alt_count == 0:
        return '0/0'
    elif alt_count == 1:
        return '0/1'
    return '1/1'

# Now we have to convert this table to a VCF format with the following fields:
# Chr - Pos - rsID - Ref - Alt - QUAL - FILTER - FORMAT - INFO
def write_record(Chrom,Pos,rsID,Ref,Alt,INFO,GT,PosID):
    """Formats given data according to gtVCF standards"""
    QUAL = '.'
    FILTER = 'PASS'
    return(Chrom,int(Pos),str(rsID),str(Ref),str(Alt),\
    QUAL,FILTER,INFO,'GT',genotype(Ref,Alt,GT,PosID))

def vcf_gen(genotype_df, info_data, index_list, dbSNP_data, inversed_data):
    """Merging data from the two DataFrames provided, creates a genotyped VCF per sample"""

    #Iterates over samples
    for i in tqdm(index_list):
        #Iterating over entries
        VCF_out = []
        count = 0
        for j in range(genotype_df.shape[0]):
            rsID=info_data.loc[j,'RsID']
            PosID=info_data.loc[j,'Name']
            Chrom=info_data.loc[j,'Chr']
            Pos=info_data.loc[j,'Position']
            Ref=dbSNP_data.loc[j,'dbsnp.ref']

            if info_data.loc[j,'Alt'] != dbSNP_data.loc[j,'dbsnp.ref']:
                Alt=info_data.loc[j,'Alt']
            else:
                Alt=info_data.loc[j,'Ref']

            if Alt=='I' or Alt=='D':
                Alt = dbSNP_data.loc[j,'dbsnp.alt']

            try:
                RSnum=re.findall('(\d+)',rsID)[0]
            except IndexError:
                RSnum=''

            GT=genotype_df.loc[j,i]
            INFO='RS='+str(RSnum)+';POS='+str(Pos)+';REV='+str(inversed_data[j])+';RGT='+GT

            VCF_out.append(write_record(Chrom,Pos,rsID,Ref,Alt,INFO,GT,PosID))

        #Let's sort VCF_out by Chrom and Pos, to output an already sorted VCF
        VCF_out = sorted(VCF_out, key=lambda x: [['0','1','2','3','4','5','6','7','8',
        '9','10','11','12','13','14','15','16','17','18','19','20','21','22','X','Y','MT','XY'].index(x[0]),
         x[1], x[2], x[3], x[4], x[5], x[6], x[7]])
        VCF_out = ['\t'.join([str(i) for i in record]) for record in VCF_out]
        VCF_out = VCF_out[5:]

        with open("FullSample_"+Sample_IDs[i]+".vcf", 'w') as v:

            #Header specification
            v.write('##fileformat=VCFv4.2\n')

            #Optional header fields
            #v.write('##'+str(Sample_IDs[i])+"\n##\n")
            #for data in Metadata:
                #v.write(data)

            v.write('##FILTER=<ID=LowConfidence,Description="RGQ<30">\n')
            v.write('##FILTER=<ID=LowConfidence,Description="DP<10">\n')
            v.write('##FILTER=<ID=MissingData,Description="GT=./.">\n')
            v.write('##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">\n')
            v.write('##INFO=<ID=RS,Number=1,Type=String,Description="Variant ID">\n')
            v.write('##INFO=<ID=POS,Number=1,Type=Integer,Description="Genomic position">\n')
            v.write('##INFO=<ID=REV,Number=1,Type=String,Description="Gene in reverse strand">\n')
            v.write('##INFO=<ID=RGT,Number=1,Type=String,Description="Raw letters in genome"\n')

            #Actual data entry
            v.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t{}\n".format(Sample_IDs[i]))
            for entry in VCF_out:
                v.write(entry+'\n')

    print("Writing complete!")

def VCF_gen_by_index(index_list):
    """Generates VCF files using only the specified columns of the gtReport"""
    vcf_gen(SNParray, INFOarray, index_list, dbSNP, Complement_vector)

def split(a, n):
    """Splits a list into n roughly equal parts"""
    k, m = divmod(len(a), n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))

if __name__ == '__main__':

    #Argument parsing
    parser = argparse.ArgumentParser(description='Converts SNP array information to a universal VCFgt file,\
    with an entry for every position of interest, regardless its discrepancy to the reference')
    parser.add_argument('--file_in', '-f', type=str, help='Array file to convert', required=True)
    parser.add_argument('--info_in', '-i', type=str, help='.txt file providing rsIDs and relevant information', required=True)
    parser.add_argument('--manifest_in', '-m', type=str, help='.csv file providing strand information from Illumina conventions', required=True)
    parser.add_argument('--dbSNP_in', '-d', type=str, help='.csv file providing dbSNP ref and alt alleles', required=True)
    parser.add_argument('--processors', '-p', type=int, default=9999, help='Number of processors for paralellization')

    args = parser.parse_args()
    file_in = args.file_in
    info_in = args.info_in
    manifest_in = args.manifest_in
    dbSNP_in = args.dbSNP_in
    processors = args.processors

    #Just a cool header º-º
    print('###########################################################    BITGENIA    ###########################################################')
    print('')
    print('Initializing array2vcf...')

    #First, let's load the genotyping file into a pandas DataFrame
    with open(file_in) as f:
        Metadata = [line for line in f][:10]
    Sample_IDs = [sample.replace('\n','') for sample in Metadata[-1].split('\t')]
    Metadata = ['##' + line for line in Metadata[:-1] if 'Samples' not in line]
    SNParray = pd.read_csv(file_in, sep='\t', skiprows=10, index_col = 0, header=None)

    #Let's now load the info table as another DataFrame
    INFOarray = pd.read_csv(info_in, sep='\t', index_col = 0, low_memory=False).loc[:,['Chr','Name','Position','RsID','Alleles']]

    #Let's load the manifest file as yet another DataFrame, holding just some columns of interest
    MANIFESTarray = pd.read_csv(manifest_in, sep=',', low_memory=False, skiprows=7).loc[:,['IlmnID','Name','IlmnStrand','SNP','SourceStrand','RefStrand']]

    #Finally, let's load dbSNP reference
    dbSNP = pd.read_csv(dbSNP_in, sep='\t')

    print('Parsing input files...')

    #Let's keep only those variants in the manifest file
    mask = np.array(SNParray.index.isin(MANIFESTarray.Name))
    SNParray = SNParray.loc[mask,:]
    mask = np.array(INFOarray.Name.isin(MANIFESTarray.Name))
    INFOarray = INFOarray.loc[mask,:]

    #Now the reciprocal...
    mask = np.array(MANIFESTarray.Name.isin(SNParray.index))
    MANIFESTarray = MANIFESTarray.loc[mask,:]

    #Let's make sure that all the arrays are in the same order
    SNParray = SNParray.sort_index()
    INFOarray = INFOarray.sort_values(by='Name')
    MANIFESTarray = MANIFESTarray.sort_values(by='Name')

    #Resets the indexes of the DataFrames
    SNParray = SNParray.reset_index(drop=False)
    INFOarray = INFOarray.reset_index(drop=True)
    MANIFESTarray = MANIFESTarray.reset_index(drop=True)

    INFOarray['Ref'] = INFOarray.Alleles.apply(get_ref)
    INFOarray['Alt'] = INFOarray.Alleles.apply(get_alt)

    #Using a mask, remove all rows in the gtReport that don't have an associated rsID
    mask = np.invert([item=='.' or pd.isnull(item) for item in INFOarray['RsID']])
    SNParray = SNParray.loc[mask,:]
    INFOarray = INFOarray.loc[mask,:]
    MANIFESTarray = MANIFESTarray.loc[mask,:]

    #Resets the indexes of the DataFrames
    SNParray = SNParray.reset_index(drop=True)
    INFOarray = INFOarray.reset_index(drop=True)
    MANIFESTarray = MANIFESTarray.reset_index(drop=True)

    print('Converting genotypes to dbSNP format...')

    #Filters and sorts dbSNP variants to match all four DataFrames
    dbSNP = dbSNP.loc[dbSNP['query'].isin(INFOarray['RsID']),:]
    dbSNP = dbSNP.drop_duplicates('query').set_index('query')
    dbSNP = dbSNP.reindex(list(INFOarray['RsID']), axis=0)
    dbSNP.reset_index(inplace=True)

    #Let's remove all variants that do not have an actual reference un dbSNP
    mask = [type(item) == str for item in dbSNP['dbsnp.ref']]
    SNParray = SNParray.loc[mask,:].reset_index()
    INFOarray = INFOarray.loc[mask,:].reset_index()
    MANIFESTarray = MANIFESTarray.loc[mask,:].reset_index()
    dbSNP = dbSNP.loc[mask,:].reset_index()

    #Let's calculate which genotypes are inverted with respect to dbSNP and store that in a vector
    IlmnStrand = MANIFESTarray.IlmnID.apply(lambda x: re.findall('_[M|P|B|T]_([R|F])_',x)[0])
    MANIFESTarray['IlmnID'] = IlmnStrand

    #We have to invert all variants that are in the + strand and in Illumina Reverse, or in the -
    #strand and in Illumina Forward.
    Manifest_complement_1 = np.all([MANIFESTarray['IlmnID']=='R',MANIFESTarray['RefStrand']=='+'], axis=0)
    Manifest_complement_2 = np.all([MANIFESTarray['IlmnID']=='F',MANIFESTarray['RefStrand']=='-'], axis=0)
    Manifest_complement = np.any([Manifest_complement_1, Manifest_complement_2], axis=0)

    #The final set of variants to reverse is the intersection between the reversed in dbSNP and
    #the manifest complement
    Complement_vector = np.all([Manifest_complement, dbSNP['Reversed']], axis=0)

    #Apply complement for each row in which the complement vector is true
    SNParray.loc[Complement_vector,[i for i in range(1,24)]] = SNParray.loc[Complement_vector,[i for i in range(1,24)]].applymap(complement)

    #In order to speed up the task, let's parallelize using a mapping technique.
    #As Pool map only takes functions with one parameter, VCF_gen_by_index is
    #just a wrapper for vcf_gen that allows this simple parallelization method.
    #The list of samples is roughly splitted into as many parts as processors the
    #machine has (with a limit of one sample per processor, obviously), and
    #conversions are performed simultaneously.
    if processors == 9999:
        processors = np.min([cpu_count(),SNParray.shape[1]-2])
    else:
        processors = int(processors)

    index_list = [i for i in range(1,SNParray.shape[1]-1)]
    index_split = list(split(index_list, processors))

    #Finally, let's parallelize and run the functions!
    if processors >1:
        print("Writing VCF files. Parallelizing in {} threads".format(processors))
    else:
        print("Writing VCF files using a single thread")

    with Pool(processors) as p:
        p.map(VCF_gen_by_index, index_split)
