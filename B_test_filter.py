#!/usr/bin/python3

"""Takes a VCF file, or a directory containing VCF files, and a text file with
variants and creates new VCF files containing only the variants from the input
VCF files that are included in the variants file. Prints some statistics about
the variants in the new VCF file.

Usage:
    btest_filter.py -r <file with variants> (-v <sample VCF> | -d <VCF dir>) -o <output dir>
"""

import os
import argparse
from multiprocessing import Pool, cpu_count

def parse_arguments():
    """Creates and returns argument parser."""
    parser = argparse.ArgumentParser(description='Filter VCF for B-Test.')
    parser.add_argument('-r', '--rs', required=True, help='file with one rs per line')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-v', '--vcf', help='VCF file')
    group.add_argument('-d', '--dir', help='directory containing VCF files')
    parser.add_argument('-o', '--output', required=True, help='output directory')
    parser.add_argument('-p', '--processes', default=cpu_count(), type=int,
                        help='number of processes to use')

    return parser.parse_args()


def load_btest_rs(rs_path):
    """Reads the file containing all the variants used in B-Test and saves them
    into a list.

    Args:
        rs_path: Path to the file containing the rs numbers of all the variants
            needed for B-Test.

    Returns:
        A list containing the variants provided by the input file.
    """

    rs_file = open(rs_path)
    rs_btest = [line.strip('\n') for line in rs_file]
    rs_file.close()
    return rs_btest

def vcf_filter(vcf_path, rs_btest, out_path):
    """Reads each line of the VCF file just once and creates a new VCF file
    only with those variants that are included in rs_btest. Returns a tuple with
    the sample name, the list of variants in the array and a dictionary with
    statistics.

    Args:
        vcf_path: Path to the input VCF file.
        rs_btest: List containing the rs numbers of all the variants needed for
            B-Test.
        out_path: Path to the output directory.

    Returns:
        A tuple with the sample name, the list of variants in the array and a
        dictionary with the number of variants in the filtered VCF that were
        not read, homozygous reference, homozygous alternative and heterozygous.
    """

    sample_name = vcf_path.split('/')[-1].split('.')[0]

    vcf_in = open(vcf_path, 'r')
    vcf_out = open(out_path.rstrip('/') + '/' + sample_name + '_filtered.vcf', 'w')

    stats = {'not_read': 0, 'hom_ref': 0, 'hom_alt': 0, 'het': 0}

    try:
        line = next(vcf_in)
        while line[0] == '#' or line == '\n':
            vcf_out.write(line)
            line = next(vcf_in)
        filtered_rs = []
        array_rs = []
        while True:
            var = line.rstrip('\n').split('\t')
            rss = var[2].split(',')
            array_rs += rss
            if any(list(map(lambda x: x in rs_btest, rss))) and var[2] not in filtered_rs:
                vcf_out.write(line)
                filtered_rs.append(var[2])
                if var[9] == './.':
                    stats['not_read'] += 1
                elif var[9] == '0/0':
                    stats['hom_ref'] += 1
                elif var[9] == '1/1':
                    stats['hom_alt'] += 1
                elif var[9] == '0/1':
                    stats['het'] += 1
            line = next(vcf_in)
    except StopIteration:
        pass

    vcf_in.close()
    vcf_out.close()

    return sample_name, array_rs, stats

def print_stats(data, rs_btest):
    """Prints statistics from each sample in a list. It takes a list with the
    output of the function vcf_filter, and returns nothing.

    Args:
        data: List of tuples, each one containing the sample name, a list with
            all the variants in the VCF file, and a dictionary with statistics
            in the first, second and third position.
        rs_btest: List containing the rs numbers of all the variants needed for
            B-Test.
    """

    for sample in data:
        print('\n==========', sample[0], '==========', '\n')
        print('Number of variants not read: ', sample[2]['not_read'])
        print('Number of homozygous reference variants: ', sample[2]['hom_ref'])
        print('Number of homozygous alternative variants: ', sample[2]['hom_alt'])
        print('Number of heterozygous variants: ', sample[2]['het'])
        print('B-Test variants: %s/%s' % (sum(sample[2].values()), len(rs_btest)))
        if sum(sample[2].values()) != len(rs_btest):
            print('\nB-Test variants not in array: ')
            for rs_id in rs_btest:
                if rs_id not in sample[1]:
                    print(rs_id)

if __name__ == '__main__':

    ARGS = parse_arguments()
    CPU_NUM = cpu_count()
    BTEST_RS = load_btest_rs(ARGS.rs)

    if ARGS.dir is not None:
        VCFS = [ARGS.dir.rstrip('/') + '/' +
                file for file in os.listdir(ARGS.dir) if file.endswith('.vcf')]

        if ARGS.processes > 0 and ARGS.processes <= CPU_NUM:
            NUM_PROC = ARGS.processes
        else:
            NUM_PROC = CPU_NUM

        def filter_mult(vcf_path):
            """Wraps the function vcf_filter for mapping with only one
            parameter.
            """
            return vcf_filter(vcf_path, BTEST_RS, ARGS.output)

        print('\nUsing %s processes' % NUM_PROC)

        with Pool(processes=NUM_PROC) as pool:
            VCF_DATA = pool.map(filter_mult, VCFS)
            print_stats(VCF_DATA, BTEST_RS)
    else:
        VCF_DATA = vcf_filter(ARGS.vcf, BTEST_RS, ARGS.output)
        print_stats([VCF_DATA], BTEST_RS)
