import myvariant, time
import numpy as np
import pandas as pd
from tqdm import tqdm
from sys import argv
from  more_itertools import unique_everseen

#file_in has to be a .bmp file; file_out is the database name
script, file_in, file_out = argv

#Initializes myvariant
mv = myvariant.MyVariantInfo()

#Reads variants from file
with open(file_in) as f:
    VARlist = [line.replace("\n","").split('\t')[1] for line in f]
VARlist = list(unique_everseen([line.split(',')[0] for line in VARlist if line.split(',')[0] != '.'][1:]))

start = time.time()

#Collect variant information and store in a dataframe
print('Number of variants: '+str(len(VARlist)))
print("Getting variant information...")

VARdf = mv.getvariants(VARlist,scope='dbsnp.rsid',fields=['dbsnp.rsid','chrom',\
'vcf.position','vcf.ref','vcf.alt'],as_dataframe=True)

VARdf=VARdf.drop_duplicates(subset='dbsnp.rsid', keep='first')

VARdf=VARdf[['dbsnp.rsid','_id', 'chrom', 'vcf.position', 'vcf.ref', 'vcf.alt']]

#Transforms the anotated DataFrame to a list of lists
anot_data = VARdf.values.tolist()

#Removes rows with uncollected data (type(nan) == float)
anot_data = [variant for variant in anot_data if type(variant[0]) != float]

print("Variants collected!")
print('Database built in '+str(round(((time.time()-start)/60),2))+' minutes')

#In order to match the database with the output of the array, fills up the space
#left by variants that were not available in dbSNP (~0,1% total).
for i in range(len(VARlist)):
    if VARlist[i] != anot_data[i][0]:
        anot_data.insert(i,[VARlist[i],'.','.','.','.','.'])

print(len(VARlist),len(anot_data))
print(sum([x[0]==y for x,y in zip(anot_data,VARlist)]))

anot_data = pd.DataFrame(anot_data)
anot_data.to_csv('GSA-'+file_out+'_dbase.tsv', sep='\t')
